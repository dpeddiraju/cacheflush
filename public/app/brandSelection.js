import React, { Component } from 'react';
import {Dropdown} from 'react-bootstrap';

export default class BrandSelection extends Component {
    constructor(props){
        super(props);
    };

    render(){
        const { handleBrand, handleRegion, handleWebsite } = this.props;
        return(
            <div className="brandSelection">
                <div className="row">
                    <div className="col-md-4"> 
                    <label>Brand</label>
                         <select onChange={e=>handleBrand(e.target.value)}>
                            <option value="Kate Spade">Kate Spade</option>
                            <option value="Coach">Coach</option>
                            <option value="Stuwart">Stuwart</option>
                        </select>
                    </div>
                    <div className="col-md-4">
                    <label>Region</label>
                     <select onChange={e=>handleRegion(e.target.value)}>
                            <option value="USA">USA</option>
                            <option value="Japan">Japan</option>
                            <option value="Canada">Canada</option>
                        </select>
                    </div>
                    <div className="col-md-4"> 
                        <label>Website</label>
                        <select onChange={e=>handleWebsite(e.target.value)}>
                            <option value="www.katespade.com">www.katespade.com</option>
                            <option value="www.coach.com">www.coach.com</option>
                            <option value="www.stuwart.com">www.stuwart.com</option>
                        </select>
                    </div>
                </div>
            </div>       
        )
    }
}
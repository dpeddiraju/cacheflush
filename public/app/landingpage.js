import React, { Component } from 'react';
import { Router, Route, Link, browserHistory, IndexRoute } from 'react-router';
import { withRouter } from 'react-router-dom';

import Header from './header';
import LeftNav from './leftnav';
class LandingPage extends Component{
    constructor(props){
        super(props);
        this.handleNavigation = this.handleNavigation.bind(this);
    };
    handleNavigation(){
        this.props.history.push('/app');
    }
    render(){
        return(
            <div>
                <Header />
                <div>
                    <div className="row">
                        <div className="col-md-1">
                            <LeftNav />
                        </div>
                        <div className="col-md-11">
                            <div className="landingPageContainer">
                                <p className="sfcc-text">Utilize this tool in order to schedule flushing cache on any of our SFCC digital properties.</p>
                                <button className="landingPageBtn" onClick={this.handleNavigation}>Schedule Cache Flush</button>
                            </div>
                        </div>
                    </div>
            </div>
            </div>
        );
    }
}

export default withRouter(LandingPage);
import React, { Component } from 'react';
import Sidebar from 'react-sidebar';
import SideBarExpand from './sideNavExpand';
export default class LeftNav extends Component{
    constructor() {
        super()
        this.state = {
            sidebarOpen: true
          };
          this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);

       }

       onSetSidebarOpen(open) {
        this.setState({ sidebarOpen: open });
      }
    render(){
        return(

                  <Sidebar
                  sidebar={<SideBarExpand />}
                  open={this.state.sidebarOpen}
                  onSetOpen={this.onSetSidebarOpen}
                  styles={{ sidebar: { background: "white" } }}
                >

                <div className="leftNav">
                    <li className="openMenu"><a onClick={()=>this.onSetSidebarOpen(true)}>>></a></li>
                    <li><a href="#"><img src="./Images/Home_onclick.png" /></a></li>
                    <li><a href="#"><img src="./Images/scheduler_onclick.png" /></a></li>
                    <li><a href="#"><img src="./Images/History_onclick.png"/></a></li>
                </div>
                </Sidebar>
        );
    }
}
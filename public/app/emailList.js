import React, { Component } from 'react';
import { FormControl } from 'react-bootstrap';

export default class EmailList extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return(
            <div className="form-">
                <label>Include Recipients on Notifications(Comma Seperated List): </label>
                <FormControl  aria-label="With textarea" placeholder="Enter Emails" onChange={(e)=>{this.props.handleEmailUpdate(e)}} className="emailFormControl" />
            </div>
        );
    }
}
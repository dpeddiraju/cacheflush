import React, { Component } from 'react';

export default class SelectRegEx extends Component {
    constructor(props){
        super(props);
        this.state={
            URL: "",
            StyleSKU: ""
        };
    }
    handleFirstBox(){
        let formOne = ["photography(S7, DAM)", "Images(jpeg, png, svg)", "Video(mpeg, mp4, mov, gif)", "CSS"];
        return(
            formOne.map( (item, index) => {
            return <li key={index}><input type="checkbox" value={item} onChange={(e)=>this.handleRegExSelectionOne(e,index)} />{item}</li>
            })
        );
    }
    handleSecondBox(){
        let formTwo = ["Homepage", "Product Listing", "Product Detail", "Checkout", "Style/SKU (Enter One Per Line Below)"];
        return(
            formTwo.map( (item, index) => {
            return <li key={index}><input type="checkbox" value={item} onChange={(e)=>{this.handleRegExSelectionTwo(e,index)}} />{item}</li>
            })
        );
    }
    handleRegExSelectionOne(e,index){
        const { handleRegExSelectPartOne } = this.props;
        handleRegExSelectPartOne(e);
     // this.setState({...this.state.selectedRegEx[e.target.value] = e.target.checked });
    }
    handleRegExSelectionTwo(e,index){
        const { handleRegExSelectPartTwo } = this.props;
        handleRegExSelectPartTwo(e);
        //this.setState({...this.state.selectedRegExSectionTwo[e.target.value] = e.target.checked});
    }
    // handleURL(e){
    //     const regEx =/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/
    //         this.setState({
    //             URL: e
    //         })
    // }
    // handleSKU(e){
    //     this.setState({
    //         StyleSKU:e
    //     })
    // }
    render(){
        return(
            <div>
                <h2>SelectRegEx</h2>

            <div className="selectRegExContainer">
                <div className="boxOne">
                    {this.handleFirstBox()}
                    <li>URL's(Enter one per Line Below)</li><textarea className="form-control rounded-0" id="exampleFormControlTextarea1" rows="5" onChange={e=>this.props.handleURL(e.target.value)}></textarea>
                </div>
                <div className="boxTwo">
                    {this.handleSecondBox()}
                    <textarea className="form-control rounded-0" id="exampleFormControlTextarea2" rows="5" onChange={e=>this.props.handleSKU(e.target.value)}></textarea>
                </div>
            </div>
        </div>
        )
    }

}
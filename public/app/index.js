import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, Link, browserHistory, IndexRoute } from 'react-router';
import { withRouter } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button} from 'react-bootstrap';
import BrandSelection from './brandSelection';
import SelectRegEx from './selectRegEx';
import Schedule from './schedule';
import Summary from './summary';
import EmailList from './emailList';
import Header from './header';
import Leftnav from './leftnav';

import './styles.css';



class App extends Component{
    constructor(props){
        super(props);
        this.state = this.getInitialState();
    }
    getInitialState(){
        return{
            showSummary: false,
            summary: {
                brand: 'Kate Spade',
                region: 'USA',
                website: 'www.katespade.com',
                selectedRegEx : {
                    "photography(S7, DAM)": false,
                    "Images(jpeg, png, svg)": false,
                    "Video(mpeg, mp4, mov, gif)": false,
                    "CSS": false
                },
                selectedRegExSectionTwo : {
                    "Homepage": false,
                    "Product Listing": false,
                    "Product Detail": false,
                    "Checkout": false,
                    "Style/SKU (Enter One Per Line Below)":false
                },
                URL: "",
                StyleSKU: "",
                startDate: new Date(),
                customDateFlag: false,
                selectType: ""
            },
            emails: [],
        }
    }
    handleConfirm(){
        console.log('....please confirm your Summary Details....');
        // Make an API Call
        //Post the entire state object  as payload
        const { summary, emails} = this.state;
        const URL = "http://localhost:3000/cacheflush";
        fetch(URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': 'http://localhost:3000/cacheflush'
            },
            body: JSON.stringify(this.state),
            })
            .then((response) => response.json())
            .then((data) => {
            console.log('Success:', data);
            })
            .catch((error) => {
            console.error('Error:', error);
            });
        this.setState({
            showSummary: true
        })
        this.props.history.push('/summary', this.state.summary)
    };
    handleResetForm(){
        //Reset all the checked inputs and clear input values
        //this.setState({showSummary: false})
        this.props.history.push('/');
    }

    handleBrand(e){
        let state = Object.assign(this.state, {
            summary: Object.assign(this.state.summary, {brand: e})
        });
        this.setState({
            state
        })
    };
    handleRegion(e){
        let state = Object.assign(this.state, {
            summary: Object.assign(this.state.summary, {region: e})
        });
        this.setState({
            state
        })
    }
    handleWebsite(e){
        let state = Object.assign(this.state, {
            summary: Object.assign(this.state.summary, {website: e})
        });
        this.setState({
            state
        })
    }

    handleRegExSelectionOne(e){
      this.setState({...this.state.summary.selectedRegEx[e.target.value] = e.target.checked });
    }
    handleRegExSelectionTwo(e){
        this.setState({...this.state.summary.selectedRegExSectionTwo[e.target.value] = e.target.checked});
    }
    handleURL(e){
        const regEx =/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/
        let state = Object.assign(this.state, {
            summary: Object.assign(this.state.summary, {URL: e})
        });
        this.setState({
            state
        })
    }
    handleSKU(e){
        let state = Object.assign(this.state, {
            summary: Object.assign(this.state.summary, {StyleSKU: e})
        });
        this.setState({
            state
        })
    }
    handleChange(date) {
        let state = Object.assign(this.state, {
            summary: Object.assign(this.state.summary, {startDate: date})
        });
        this.setState({
            state
        })
      };
    handleSelectTime(e){

       let state = Object.assign(this.state, {
        summary: Object.assign(this.state.summary, {selectType: e})
        });
        this.setState({
            state
        });

       e === 'Custom'? this.setState({ ...this.state.summary.customDateFlag= true}) : this.setState({...this.state.summary.customDateFlag= false, ...this.state.summary.startDate= new Date()});
    
    }
    handleEmailChange(e){
        const emailRegEx = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/
        let emails = e.target.value;
        this.setState({
            emails: emails
        })

    }
    render(){
        return(
            <div>
              <Header />
                <div className="row">
                  <div className="col-md-1">
                    <Leftnav/>
                  </div>
                  <div className="col-md-11">
                  <div className="container">
                    
                        <BrandSelection handleBrand={this.handleBrand.bind(this)} handleRegion={this.handleRegion.bind(this)} handleWebsite={this.handleWebsite.bind(this)} />
                    <div>
                      <div>
                        <SelectRegEx handleRegExSelectPartOne={this.handleRegExSelectionOne.bind(this)} handleRegExSelectPartTwo={this.handleRegExSelectionTwo.bind(this)} handleURL={this.handleURL.bind(this)} handleSKU={this.handleSKU.bind(this)} />
                      </div>
                      <div className="scheduleMainContainer">
                        <Schedule handleStartDate={this.handleChange.bind(this)} handleSelectTime={this.handleSelectTime.bind(this)} startDate={this.state.summary.startDate} customDateFlag={this.state.summary.customDateFlag} />
                      </div>
                      <div className="emailList">
                        <EmailList handleEmailUpdate={this.handleEmailChange.bind(this)} />
                      </div>   
                      <div className="scheduleMainContainer">
                        {  this.state.showSummary &&
                            <Summary data={this.state.summary} /> 
                        }
                      </div>
                      <div className="btn"> 
                        <button className="mr-2" onClick={this.handleResetForm.bind(this)}>Cancel</button>
                        <button onClick={this.handleConfirm.bind(this)}>Confirm</button>
                      </div>
                 </div>
                </div>
               </div>
            </div>
        </div>
        );
    }
}

// const dom = document.getElementById('cache-flush-bootstrap');
// ReactDOM.render(
//     <App/>,
//     dom
// );

export default withRouter(App)
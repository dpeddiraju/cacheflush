import React, { Component } from 'react';
import Header from './header';
import LeftNav from './leftnav';

export default class Summary extends Component {
    constructor(props){
        super(props);
        this.handleGoHome = this.handleGoHome.bind(this);
        this.viewFlushSchedule = this.viewFlushSchedule.bind(this);
    }
    handleGoHome(){
        this.props.history.push('/')
    }
    viewFlushSchedule(){

    }
    renderSelectRegExPartOne(data){
        let selectedInputs = [];
        for (const key of Object.keys(data)) {
            if(data[key] === true){
                selectedInputs.push(key);
            }
        }
        return(
        selectedInputs.map((value, index)=><li key={index}>{value}</li>)
        )
    }
    render(){
        const { brand, region, website, URL, StyleSKU, selectedRegEx, selectedRegExSectionTwo, startDate} = this.props.location.state;
        return(

            <div>
             <Header />
             <div className="row">
                 <div className="col-md-1">
                     <LeftNav />
                 </div>
                 <div className="col-md-11">
                    <p className="summaryHeading">Congratulations, cache flush  has successfully been Scheduled!</p>
                    <h2 className="summaryHeadingh2">Summary</h2>
                   <div className="summaryContainer">
                        <div className="row">
                            <div className="col-md-4">
                                <li>Brand: {brand}</li>
                                <li>Region: {region}</li>
                                <li>Website: {website}</li>
                            </div>

                            <div className="col-md-4">
                                {this.renderSelectRegExPartOne(selectedRegEx)}
                                 <li>URL: {URL}</li>
                            </div>
                                
                            <div className="col-md-4">
                                {this.renderSelectRegExPartOne(selectedRegExSectionTwo)}
                                <li>URL: {StyleSKU} </li>
                            </div>
                        </div>

                     <p className="scheduledDateAndTime">Cache Flush Scheduled For: {startDate.toString()} </p>
                    </div>
                    <div className="summaryButtons">
                      <button onClick={this.handleGoHome}>Go Home</button>
                      <button className="viewFlushScheduleButton" onClick={this.viewFlushSchedule}>View Flush Schedule</button>
                    </div>
                   </div>
                   
                </div>
                 
             </div>
        )
    }
}
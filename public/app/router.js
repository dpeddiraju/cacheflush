import React from 'react';
import ReactDOM from 'react-dom';
import { Route, Link, IndexRoute, BrowserRouter as Router, Switch} from 'react-router-dom';
import LandingPage from './landingpage';
import App from './index';
import Summary from './summary';
import Notfound from './notFound';
const dom = document.getElementById('cache-flush-bootstrap');
const routing = (
    <Router>
      <div>
        <Switch>
        <Route exact path="/" component={LandingPage} />
        <Route  path="/app" component={App} />
        <Route path="/summary" component={Summary} />
        <Route component={Notfound} />
        </Switch>
      </div>
    </Router>
);
ReactDOM.render(routing, dom);
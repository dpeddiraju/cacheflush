import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
 
export default class Schedule extends Component {
    constructor(props){
        super(props);
    }
 
    render(){
        const { customDateFlag } = this.props;
        return(
            <div>
                 <h2>Schedule</h2>
                 <div className="scheduleContainer">
                 <li className="alignment">
                     Select Type: 
                     <select onChange={e=>this.props.handleSelectTime(e.target.value)}>
                        <option>Select</option>
                        <option value="Custom">Custom</option>
                        <option value="Hourly">Hourly</option>
                        <option value="Daily">Daily</option>
                        <option value="Weekly">Weekly</option>
                        <option value="Monthly">Monthly</option>
                        <option value="Yearly">Yearly</option>
                     </select>
                 </li>
                 { customDateFlag &&
                 <li className="alignment">
                     Date/Time:
                     <DatePicker
                        selected={this.props.startDate}
                        onChange={this.props.handleStartDate}
                        showTimeSelect
                        dateFormat="Pp"
                     />  
                </li>
                }
             </div>
            </div>
        );
    }
  }

var express = require('express');
var router = express.Router();

/* GET users listing. */
router.post('/', function(req, res, next) {
  // we got data from UI 
  // decide what we need to do with this data
  // like how we handle cahce flush or purging
  // how to store this data in database
  const { summary, emails } = req.body;
  console.log('..........Summary Object.......', summary);
  console.log('..........Emails.....Object', emails);

  res.setHeader('Content-Type', 'application/json');
  res.setHeader('Access-Control-Allow-Methods', 'GET');
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.send('Reached the Node API');
});

module.exports = router;

var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  if(process.env.NODE_ENV === 'production') {
    console.log('We are running in production mode');
  } else {
    console.log('We are running in development mode');
  }
  res.send('index', { title: 'Cache Flush initial skeleton' });
});

module.exports = router;